package mis.cursos.springproductos_BACK02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringProductosBack02Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringProductosBack02Application.class, args);
	}

}
