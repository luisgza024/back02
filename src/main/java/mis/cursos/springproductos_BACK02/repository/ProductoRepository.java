package mis.cursos.springproductos_BACK02.repository;

import mis.cursos.springproductos_BACK02.model.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {

}
